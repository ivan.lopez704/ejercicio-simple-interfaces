package com.itswa.interfacespoo.model;

public class Serie extends ProductRentable implements Entregable{

    private final static int DEFAULT_NUM_TEMPORADAS = 3;
    private final static String DEFAULT_CREADOR="No tiene creador";

    private int numTemporadas;
    private String creador;

    public Serie() {
        super();
    }

    public Serie(String titulo, String creador) {
        super();
        this.titulo = titulo;
        this.creador = creador;
        this.numTemporadas = DEFAULT_NUM_TEMPORADAS;
    }

    public Serie(String titulo, int numTemporadas, String genero, String creador) {
        this(titulo, creador);
        this.numTemporadas = numTemporadas;
        this.genero = genero;
    }

    public int getNumTemporadas() {
        return numTemporadas;
    }

    public void setNumTemporadas(int numTemporadas) {
        this.numTemporadas = numTemporadas;
    }


    public String getCreador() {
        return creador;
    }

    public void setCreador(String creador) {
        this.creador = creador;
    }

    @Override
    public String toString() {
        return "Serie{" +
                "titulo='" + titulo + '\'' +
                ", numTemporadas=" + numTemporadas +
                ", entregado=" + entregado +
                ", genero='" + genero + '\'' +
                ", creador='" + creador + '\'' +
                '}';
    }

    @Override
    public void entregar() {
        this.entregado = true;
    }

    @Override
    public void devolver() {
        this.entregado = false;
    }

    @Override
    public boolean isEntregado() {
        return this.entregado;
    }

    @Override
    public boolean compareTo(Object a) {
        Serie datoCompare = (Serie) a;
        if(this.numTemporadas == datoCompare.numTemporadas)
            return true;
        return false;
    }
}
