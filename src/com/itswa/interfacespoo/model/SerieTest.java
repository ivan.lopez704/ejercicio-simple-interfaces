package com.itswa.interfacespoo.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SerieTest {

    Serie serie;

    @BeforeEach
    void setUp() {
        serie = new Serie("The big bang theory",12,"comedy","Larry ...");
    }

    @Test
    void createObjectTest() {
        assertAll("This is test for get's",
                ()-> assertEquals(serie.getTitulo(),"The big bang theory"),
                ()-> assertEquals(serie.getNumTemporadas(),12),
                ()-> assertEquals(serie.getGenero(),"comedy"),
                ()-> assertEquals(serie.getCreador(),"Larry ..."));
    }

    @Test
    void setTest(){
        serie.setTitulo("Malcom in the middle");
        serie.setCreador("I dont jnow");
        serie.setGenero("funny things");
        serie.setNumTemporadas(9);

        assertAll("This is test for set",
                ()-> assertEquals(serie.getCreador(),"I dont jnow"),
                ()-> assertEquals(serie.getTitulo(),"Malcom in the middle"),
                ()-> assertEquals(serie.getGenero(),"funny things"),
                ()-> assertEquals(serie.getNumTemporadas(),9));
    }

    @Test
    void entregar() {
        serie.entregar();
        assertEquals(serie.isEntregado(),true);
    }

    @Test
    void devolver() {
        serie.devolver();
        assertEquals(serie.isEntregado(),false);
    }

    @Test
    void isEntregado() {
        assertEquals(serie.isEntregado(),false);
    }

    @Test
    void compareTo() {
        Serie compareObject = new Serie("Good Friend",5,"drama","Fox");
        assertEquals(serie.compareTo(compareObject),false);
        compareObject.setNumTemporadas(12);
        assertEquals(serie.compareTo(compareObject),true);
    }

}