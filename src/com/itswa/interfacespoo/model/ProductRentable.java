package com.itswa.interfacespoo.model;

abstract public class ProductRentable {
    protected final static String DEFAULT_TITULO="No tiene titulo";
    private final static String DEFAULT_GENERO="Sin genero";
    private final static boolean DEFAULT_ENTREGADO= false;

    protected String titulo;
    protected String genero;
    protected boolean entregado;

    public ProductRentable() {
        this.titulo = DEFAULT_TITULO;
        this.genero = DEFAULT_GENERO;
        this.entregado = DEFAULT_ENTREGADO;
    }

    public ProductRentable(String titulo, String genero) {
        this();
        this.titulo = titulo;
        this.genero = genero;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    abstract public String toString();

}
