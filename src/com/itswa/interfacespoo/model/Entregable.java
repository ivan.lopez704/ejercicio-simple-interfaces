package com.itswa.interfacespoo.model;

public interface Entregable {
    public void entregar();
    public void devolver();
    public boolean isEntregado();
    public boolean compareTo(Object a);

}
