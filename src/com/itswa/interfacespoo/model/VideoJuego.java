package com.itswa.interfacespoo.model;

public class VideoJuego implements  Entregable {
    private final static String DEFAULT_TITULO="No tiene titulo";
    private final static int DEFAULT_HORAS_ESTIMADAS = 10;
    private final static String DEFAULT_GENERO="Sin genero";
    private final static String DEFAULT_COMPANIA="No tiene compañia";
    private final static boolean DEFAULT_ENTREGADO= false;

    private String titulo;
    private int horasEstimadas;
    private boolean entregado;
    private String genero;
    private String compania;

    public VideoJuego() {
    }

    public VideoJuego(String titulo, int horasEstimadas) {
        this.titulo = titulo;
        this.horasEstimadas = horasEstimadas;
        this.entregado = DEFAULT_ENTREGADO;
        this.genero = DEFAULT_GENERO;
        this.compania = DEFAULT_COMPANIA;
    }

    public VideoJuego(String titulo, int horasEstimadas, String genero, String compania) {
        this(titulo,horasEstimadas);
        this.genero = genero;
        this.compania = compania;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getHorasEstimadas() {
        return horasEstimadas;
    }

    public void setHorasEstimadas(int horasEstimadas) {
        this.horasEstimadas = horasEstimadas;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getCompania() {
        return compania;
    }

    public void setCompania(String compania) {
        this.compania = compania;
    }


    @Override
    public void entregar() {
        this.entregado = true;
    }


    @Override
    public void devolver() {
        this.entregado = false;
    }

    @Override
    public boolean isEntregado() {
        return this.entregado;
    }

    @Override
    public boolean compareTo(Object a) {
        VideoJuego datoCompare = (VideoJuego) a;
        if(this.horasEstimadas == datoCompare.horasEstimadas)
            return true;
        return false;
    }

    @Override
    public String toString() {
        return "VideoJuego{" +
                "titulo='" + titulo + '\'' +
                ", horasEstimadas=" + horasEstimadas +
                ", entregado=" + entregado +
                ", genero='" + genero + '\'' +
                ", compania='" + compania + '\'' +
                '}';
    }
}
